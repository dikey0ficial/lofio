# Building from source.

OK, you've chosen hard way. Spoiler: there are different instructions for Unix-like and Windows

## Building on Unix-like OS

### Building dependencies

For building these tools are needed:

 - Go 1.18 or newer
 - [libvlc-dev](https://github.com/adrg/libvlc-go#prerequisites)
 - [Gio requirements](https://gioui.org/install/)
 - Git

### Instruction

1. Clone repo and `cd` into it
2. Run `make`

### Notes

 - Don't even try cross-compile for Windows. It's a pain.
 - If you want to disable mpris integration, run `make GOTAGS=-tags=nompris`

## Building on Windows

### Building dependencies

For building these tools are needed:

  - Go 1.18 or newer
  - NuGet
  - Git
  - CMake
  - MinGW make, gcc and g++

### Instruction

1. Clone repo and `cd` into it
2. Run `.\winbuild.bat`

### Notes

 - There are some environmental variables that can modify behaviour:
	 + `Arch` defines target architecture (only `x64` and `x32` are aviable). Default: x64
	 + `CC` and `CXX` define C and C++ compilers. Default: gcc and g++
	 + `BinDir` and `LofioOutputFileName` define output binary file's directory and name. Default: %BuildDir%\bin and Lofio-%Arch%.exe
	 + `LofioVersion` defines Lofio version. By default it takes the latest git tag.
 - Somewhy it doesn't compile for x86. Really sorry.
 - There's no miniplayer integration because it's made with D-Bus.
