CC=gcc
CXX=g++
GOC=go
GOFLAGS=
GO_LDFLAGS=
GOTAGS=
BUILDDIR=.
BINEXT=

all:
	$(GOC) build -ldflags="$(GO_LDFLAGS)" -tags="$(GOTAGS)" -o="$(BUILDDIR)/lofio$(BINEXT)" $(GOFLAGS) ./gui/lofio 
