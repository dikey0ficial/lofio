module codeberg.org/hrueschwein/lofio

go 1.18

require (
	fyne.io/systray v1.10.0
	gioui.org v0.0.0-20220607102828-5cf916c0753c
	gioui.org/x v0.0.0-20220609132952-25f67b5b7e87
	github.com/BurntSushi/toml v1.1.0
	github.com/adrg/libvlc-go/v3 v3.1.5
	github.com/godbus/dbus/v5 v5.0.6
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/sqweek/dialog v0.0.0-20220809060634-e981b270ebbf
	gopkg.in/goyy/goyy.v0 v0.0.0-20190218140538-82e7740e526e
)

require (
	gioui.org/cpu v0.0.0-20210817075930-8d6a761490d2 // indirect
	gioui.org/shader v1.0.6 // indirect
	git.wow.st/gmp/jni v0.0.0-20210610011705-34026c7e22d0 // indirect
	github.com/TheTitanrain/w32 v0.0.0-20180517000239-4f5cfb03fabf // indirect
	github.com/benoitkugler/textlayout v0.1.1 // indirect
	github.com/gioui/uax v0.2.1-0.20220325163150-e3d987515a12 // indirect
	github.com/go-text/typesetting v0.0.0-20220411150340-35994bc27a7b // indirect
	github.com/tevino/abool v1.2.0 // indirect
	golang.org/x/exp v0.0.0-20210722180016-6781d3edade3 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/text v0.3.7 // indirect
)
