@echo off

if "%CC%" == "" (
	set "CC=gcc"
	set CCChanged=1
)

if "%CXX%" == "" (
	set "CXX=g++"
	set CXXChanged=1
)

set GOOS=windows

echo *   [  0%%] preparing... 
if exist ".\build" (
	rmdir /s /q ".\build" 1>nul
)

mkdir ".\build" 1>nul
cd .\build

cd > temp.txt
set /p BuildDirPath=< temp.txt

echo *   [ 10%%] copying source... 
robocopy .. . /mir /xd ..\build ..\.git ..\winbuild.bat >log.txt 2>&1

echo *   [ 30%%] installing VideoLAN.LibVLC.Windows... 
nuget install VideoLAN.LibVLC.Windows >log.txt 2>&1
dir /AD /B 1>temp.txt
findstr "VideoLAN.LibVLC" temp.txt > temp2.txt

set /p LibVLCRoot=<temp2.txt

echo *   [ 50%%] installing gogio...

go env GOBIN >temp.txt
set /p GOBIN=<temp.txt
if "%GOBIN%" == "" (
	if "%GOPATH%" == "" (
		go env GOPATH >temp.txt
		set /p GOPATH=<temp.txt
	)
	set "GOBIN=%GOPATH%\bin"		
	)
)

go install "gioui.org/cmd/gogio@latest" >log.txt 2>&1

echo *   [ 70%%] preparing to build... 

if "%Arch%" == "" (
	set "Arch=x64"
	set ArchChanged=1
)

set GOARCH=amd64
if "%Arch%" == "x86" (
	set GOARCH=386
)

go mod tidy -e >log.txt 2>&1

if "%LofioVersion%" == "" (
	git describe --tags --abbrev=0 1>temp.txt
	set /P LofioVersion=<temp.txt
)

if "%BinDir%" == "" (
	set "BinDir=%BuildDirPath%\bin"
)

if not exist "%BinDir%" mkdir "%BinDir%"


echo *   [ 80%%] building... 
set "SDKPath=%BuildDirPath%\%LibVLCRoot%\build\%Arch%"
set "CGO_CFLAGS=-I%SDKPath%\include"
set "CGO_LDFLAGS=-L%SDKPath%"

if "%LofioOutputFileName%" == "" set "LofioOutputFileName=lofio-%Arch%.exe"

%GOBIN%\gogio.exe -tags="nompris" -ldflags="-X main.appVersion=%LofioVersion%" -target=windows -icon=icon.png -o="%BinDir%\%LofioOutputFileName%" ".\gui\lofio" >log.txt 2>&1


echo *   [ 90%%] cleaning... 
set GOPATH=
set GOBIN=
set GOOS=
set GOARCH=
set CGO_LFLAGS=
set CGO_CFLAGS=
if "%CCChanged%" == "1" set CC=
if "%CXXChanged%" == "1" set CXX=
if "%ArchChanged%" == "1" set Arch=
cd ..

if exist "%BinDir%\%LofioOutputFileName%" (
	echo *   [100%%] build: OK
	echo Binary file is in %BinDir%
) else (
	echo *   [100%%] build: FAILED
	echo Look at build\log.txt
)
