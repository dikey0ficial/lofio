![(lofio icon)](icon.png "lofio icon")

# Lofio — Lofi hip-hop radio player

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg "MIT")](https://opensource.org/licenses/MIT "MIT") [![Language: Go](https://img.shields.io/static/v1?message=Go&labelColor=5c5c5c&color=00ADD8&label=Language "Go")](https://go.dev/ "Go") [![GUI: Gio](https://img.shields.io/static/v1?message=Gio&labelColor=5c5c5c&color=fff&label=GUI "Gio")](https://gioui.org/ "Gio")

# ARCHIVED

...due to being a piece of schizofrenic unmaintainable code.
Now I'm developing (yes, much slower) Qt radio player called [radian](https://codeberg.org/hrueschwein/radian)

# ...

It can hide in tray and integrate with miniplayer (based on DBus, org.mpris.MediaPlayer2)

## Installation

You can take pre-built binary from [Releases](https://codeberg.org/hrueschwein/lofio/releases), [add repo](https://software.opensuse.org/package/lofio) for your distro (currently only openSUSE is supported, but I'll try to add support for Fedora, Debian and Arch) or [build from source](BUILD.md)

There's only one runtime dependecy — LibVLC. Other are precompiled in binary file, thanks to Golang

## Technologies

You can find used libraries in [go.mod](go.mod) file and [specification](SPECS.md)
