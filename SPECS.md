\# Technical specification

I wrote it to make developing Lofio easier: strict specifications are good for developing. However, sometimes I ignore them. It's bad, but I'll refactor later (or no))) )

## About Lofio

Lofio is simple application that plays internet radios, primarly lo-fi hiphop

## Expected features

 - [x] Playing internet radiostations 
	 - [x] Ability to pause and resume playing
	 - [x] Changing volume
 	
 - [x] Hiding in tray
	 - [x] Ability to disable in...
		 - [x] `config.toml`
		 - [x] settings

 - [x] Managing stations
	 - [x] From `config.toml`
	 - [x] From settings
		 - [x] Adding stations
		 - [x] Deleting stations
		 - [x] Editing stations
		 
 - [x] Hotkeys
	 - [x] Local
		 - [x] Customizing
			 - [x] `config.toml`
			 - [x] Settings (Manual input)
	 - [x] Actions
		 - [x] Play/Pause
		 - [x] Switch tab  (back)
		 - [x] Volume up/down
		 - [x] Quit app (as in tray's quit)

 - [x] Integration with MPRIS
	 - [x] Ability to pause/resume
	 - [x] Geting info about station
	 - [ ] Track metadata
	 - [ ] Configuring MPRIS

 - [x] Prinitng errors
	 - [x] in console
	 - [x] using GUI

 - [ ] Localization
	 - [ ] Changing language
		 - [ ] Auto-detection
		 - [ ] Manual
	 - [ ] Aviable languages:
		 - [x] English
		 - [ ] Russian <!-- I don't know other langs good enough -->

 - [x] Theming
	 - [x] Changing theme
		 - [x] Auto-detection
		 - [x] Manual
	 - [x] Themes
		 - [x] Light
		 - [x] Dark

 - [ ] Multimedia metadata


## Interface

In top of application there are tabs: radiostations and settings.

### Radiostation view

There's big button "Play/Pause" and volume regulator.

### Settings

There's list of some settings (such as hiding to tray or not) and information about app

## Tecnhologies:

 - Language: Go
 - GUI: [Gio](https://gioui.org)
 - Audio: [adrg/libvlc-go](https://github.com/adrg/libvlc-go)
 - Tray: [fyne.io/systray](https://github.com/fyne-io/systray)
 - D-Bus integration: most propabely [godbus/dbus](https://github.com/godbus/dbus)
