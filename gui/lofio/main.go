package main

import (
	"github.com/sqweek/dialog"

	"fmt"
	"os"
	rdebug "runtime/debug"
)

var (
	appVersion   = ""
	buildInfo = rdebug.BuildInfo{}

	inits = map[string]func() error{
		"conf":  confInit,
		"log":   logInit,
		"audio": audioInit,
	}

	mains = map[string]func() error{
		"app": newWindow,
	}

	stopCh = make(chan struct{})
)

func _init() {
	for name, fn := range inits {
		if err := fn(); err != nil {
			errl.Println(name, ":", err)
			dialog.Message("Error initiating component `" + name + "`").Error()
			os.Exit(1)
		}
	}
	bi, ok := rdebug.ReadBuildInfo()
	if ok {
		buildInfo = *bi
		if appVersion == "" {
			appVersion = buildInfo.Main.Version
		}
		if conf.debug {
			debl.Printf("BuildInfo: %v\n", buildInfo)
			debl.Println("Deps:")
			for _, dep := range buildInfo.Deps {
				debl.Println(dep)
			}
			debl.Println("========================================================")
			fmt.Print("\n\n\n")
		}

	}
}

func main() {
	_init()

	for n, m := range mains {
		go func(n string, m func() error) {
			err := m()
			if err != nil {
				errl.Println(n, ":", err)
				dialog.Message("Error running component `" + n + "`").Error()
			}
		}(n, m)
	}
	trayRun()
	close(stopCh)
}
