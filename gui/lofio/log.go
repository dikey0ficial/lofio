package main

import (
	"io"
	"log"
	"os"
)

var (
	// debug log
	debl = log.New(os.Stderr, "[ DEBUG ]\t", log.Ldate|log.Ltime|log.Lshortfile)
	errl = log.New(os.Stderr, "[ ERROR ]\t", log.Ldate|log.Ltime|log.Lshortfile)
)

func logInit() error {
	if !conf.debug {
		debl.SetOutput(io.Discard)
	}
	return nil
}
