// Tabs implements TabStyle struct for Gio (https://gioui.org/)
// TabStyle are based on gioui.org/widget/material.TabStyle
package tabs

import (
	"image"
	"image/color"

	"gioui.org/io/semantic"
	"gioui.org/layout"
	"gioui.org/op/clip"
	"gioui.org/op/paint"
	"gioui.org/text"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
)

type TabStyle struct {
	checkable
	Key   string
	Group *widget.Enum
}

// Tab returns a Tab with a label. The key specifies
// the value for the Enum.
func Tab(th *material.Theme, group *widget.Enum, key, label string) TabStyle {
	return TabStyle{
		Group: group,
		checkable: checkable{
			Label: label,

			Theme:    th,
			TextSize: th.TextSize * 14.0 / 16.0,
			Size:     26,
			shaper:   th.Shaper,
		},
		Key: key,
	}
}

// Layout updates enum and displays the radio button.
func (r TabStyle) Layout(gtx layout.Context) layout.Dimensions {
	hovered, hovering := r.Group.Hovered()
	focus, focused := r.Group.Focused()
	return r.Group.Layout(gtx, r.Key, func(gtx layout.Context) layout.Dimensions {
		semantic.RadioButton.Add(gtx.Ops)
		highlight := hovering && hovered == r.Key || focused && focus == r.Key
		return r.layout(gtx, r.Group.Value == r.Key, highlight)
	})
}

type checkable struct {
	Label    string
	Font     text.Font
	TextSize unit.Sp

	Theme  *material.Theme
	Size   unit.Dp
	shaper text.Shaper
}

func (c *checkable) layout(gtx layout.Context, checked, hovered bool) layout.Dimensions {
	dims := layout.Stack{Alignment: layout.Center}.Layout(gtx,
		layout.Stacked(func(gtx layout.Context) layout.Dimensions {
			size := gtx.Dp(c.Size)
			dims := layout.Dimensions{
				Size: image.Point{X: size * 4, Y: size},
			}

			var col color.NRGBA

			if checked {
				col = c.Theme.Palette.ContrastBg
			} else if hovered {
				col = internalMulAlpha(c.Theme.Palette.ContrastBg, 100)
			} else {
				col = internalMulAlpha(c.Theme.Palette.Fg, 50)

			}

			b := image.Rectangle{Max: image.Pt(size*4, size)}
			paint.FillShape(gtx.Ops, col, clip.UniformRRect(b, 10).Op(gtx.Ops))

			return dims
		}),
		layout.Stacked(func(gtx layout.Context) layout.Dimensions {
			var col color.NRGBA

			if checked {
				col = c.Theme.Palette.ContrastFg
			} else {
				col = c.Theme.Palette.Fg
			}

			return layout.UniformInset(2).Layout(gtx, func(gtx layout.Context) layout.Dimensions {
				paint.ColorOp{Color: col}.Add(gtx.Ops)
				return widget.Label{}.Layout(gtx, c.shaper, c.Font, c.TextSize, c.Label)
			})
		}),
	)
	return dims
}

// internalMulAlpha applies the alpha to the color.
// it's a copy of gioui.org/internal/f32color.MulAplpha
func internalMulAlpha(c color.NRGBA, alpha uint8) color.NRGBA {
	c.A = uint8(uint32(c.A) * uint32(alpha) / 0xFF)
	return c
}
