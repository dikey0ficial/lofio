package main

import (
	"gioui.org/app"
	"gioui.org/io/key"
	"github.com/BurntSushi/toml"
	"os"
)

type Station struct {
	Name string
	Link string
}

type ConfigStruct struct {
	debug bool

	Stations                 []Station     `toml:"stations"`
	CloseAppWhenWindowCloses bool          `toml:"close_app_when_window_closes"`
	Theme                    int           `toml:"theme"` // 0 — try to detect, 1 — light, 2 — dark
	Hotkeys                  HotkeysConfig `toml:"hotkeys"`
}

type HotkeysConfig struct {
	PlayPause     string `toml:"play_pause"`
	SwitchTab     string `toml:"switch_tab"`
	SwitchTabBack string `toml:"switch_tab_back"`
	VolumeUp      string `toml:"volume_up"`
	VolumeDown    string `toml:"volume_down"`
	QuitApp       string `toml:"quit_app"`

	// idea: list scrollers
}

var (
	defaultConf = ConfigStruct{
		debug: false,
		Stations: []Station{
			{"Planet LoFi", "https://www.internet-radio.com/servers/tools/playlistgenerator/?u=http://198.245.60.88:8080/listen.pls?sid=1&t=.m3u"},
			{"Chillsky", "https://www.internet-radio.com/servers/tools/playlistgenerator/?u=http://hyades.shoutca.st:8043/autodj&t=.m3u"},
			{"Boolout", "http://ec2.yesstreaming.net:1910/stream"},
		},
		CloseAppWhenWindowCloses: false,
		Theme: 0,
		Hotkeys: HotkeysConfig{
			PlayPause:     "Ctrl-P",
			SwitchTab:     "Ctrl-Tab",
			SwitchTabBack: "Ctrl-Shift-Tab",
			VolumeUp:      "Ctrl-" + key.NameUpArrow,
			VolumeDown:    "Ctrl-" + key.NameDownArrow,
			QuitApp:       "Ctrl-Q",
		},
	}
	conf = ConfigStruct{}

	dataDir, _ = app.DataDir()
	// yes, it would be better if lofioDataDir and configPath were const, but dataDir is var
	lofioDataDir = dataDir + string(os.PathSeparator) + "lofio"
	configPath   = lofioDataDir + string(os.PathSeparator) + "config.toml"
)

func confInit() error {
	err := confLoad()
	if err != nil {
		return err
	}

	var changed bool

	if conf.Stations == nil || len(conf.Stations) == 0 {
		conf.Stations = defaultConf.Stations
		changed = true
	}

	if conf.Hotkeys == (HotkeysConfig{}) {
		conf.Hotkeys = defaultConf.Hotkeys
		changed = true
	}

	if changed {
		err := confSave()
		if err != nil {
			return err
		}
	}

	if s, _ := os.LookupEnv("DEBUG"); s == "1" || s == "true" {
		conf.debug = true
	}

	return nil
}

func confLoad() error {
	f, err := os.Open(configPath)
	if err != nil {
		if os.IsNotExist(err) {
			os.MkdirAll(lofioDataDir, 0777)
			f, err = os.Create(configPath)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}

	defer f.Close()

	_, err = toml.NewDecoder(f).Decode(&conf)
	return err
}

func confSave() error {
	f, err := os.Create(configPath)
	if err != nil {
		return err
	}

	enc := toml.NewEncoder(f)
	enc.Indent = "\t"
	return enc.Encode(conf)
}
