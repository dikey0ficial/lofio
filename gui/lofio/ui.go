package main

import (
	"codeberg.org/hrueschwein/lofio/gui/lofio/tabs"
	"gioui.org/app"
	"gioui.org/font/gofont"
	"gioui.org/io/key"
	"gioui.org/io/system"
	"gioui.org/layout"
	"gioui.org/op"
	"gioui.org/op/paint"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"gioui.org/x/component"
	"gioui.org/x/pref/theme"
	"github.com/skratchdot/open-golang/open"
	"github.com/sqweek/dialog"
	goyystr "gopkg.in/goyy/goyy.v0/util/strings"

	"fmt"
	"image/color"
	"strconv"
)

// ==================== type aliases ===================

type (
	C = layout.Context
	D = layout.Dimensions
)

// =====================================================

var (
	lightPalette = material.Palette{
		Bg:         color.NRGBA{0xff, 0xff, 0xff, 0xff},
		Fg:         color.NRGBA{0x00, 0x00, 0x00, 0xff},
		ContrastBg: color.NRGBA{0x61, 0x56, 0xff, 0xff}, // #6156ff
		ContrastFg: color.NRGBA{0xff, 0xff, 0xff, 0xff},
	}
	darkPalette = material.Palette{
		Bg:         color.NRGBA{0x2b, 0x2a, 0x33, 0xff}, // #2b2a33
		Fg:         color.NRGBA{0xff, 0xff, 0xff, 0xff},
		ContrastBg: color.NRGBA{0x61, 0x56, 0xff, 0xff},
		ContrastFg: color.NRGBA{0xff, 0xff, 0xff, 0xff},
	}
	chosenTab string
)

// UI is abstraction for all interface
type UI struct {
	Theme            *material.Theme
	TabChooser       *TabChooser
	RadioActivity    *RadioActivity
	SettingsActivity *SettingsActivity
}

// NewUI returns new *UI with std values
func NewUI() *UI {
	ui := new(UI)
	ui.Theme = material.NewTheme(gofont.Collection())
	switch (conf.Theme) {
	case 0:
		is, _ := theme.IsDarkMode()
		if is {
			ui.Theme.Palette = darkPalette
		} else {
			ui.Theme.Palette = lightPalette
		}
	case 1:
		ui.Theme.Palette = lightPalette
	case 2:
		ui.Theme.Palette = darkPalette
	default:
		conf.Theme = 0
		confSave()
	}

	ui.TabChooser = NewTabChooser(ui.Theme)
	ui.TabChooser.Tabs = append(ui.TabChooser.Tabs, tabs.Tab(
		ui.Theme,
		ui.TabChooser.Group,
		"_settings",
		"Settings",
	))
	if len(conf.Stations) != 0 {
		if chosenTab == "" {
			chosenTab = conf.Stations[0].Name
		}
		ui.TabChooser.Group.Value = chosenTab
	}
	for _, station := range conf.Stations {
		ui.TabChooser.Tabs = append(
			ui.TabChooser.Tabs,
			tabs.Tab(
				ui.Theme,
				ui.TabChooser.Group,
				station.Name,
				goyystr.Abbr(station.Name, 12),
			),
		)
	}

	ui.RadioActivity = NewRadioActivity(ui.Theme)
	ui.SettingsActivity = NewSettingsActivity(ui.Theme)

	return ui
}

// Run processes window
func (ui *UI) Run(w *app.Window) error {
	var ops op.Ops

	for {
		select {
		case e := <-w.Events():
			switch e := e.(type) {
			case system.FrameEvent:
				gtx := layout.NewContext(&ops, e)

				key.InputOp{
					Tag: w,
					Keys: key.Set(
						conf.Hotkeys.PlayPause +
							"|" + conf.Hotkeys.SwitchTab +
							"|" + conf.Hotkeys.SwitchTabBack +
							"|" + conf.Hotkeys.VolumeUp +
							"|" + conf.Hotkeys.VolumeDown +
							"|" + conf.Hotkeys.QuitApp,
					),
				}.Add(gtx.Ops)

				for _, e := range gtx.Events(w) {
					switch e := e.(type) {
					case key.Event:

						if e.State != key.Release {
							continue
						}

						debl.Println(hotkey2text(e))
						switch hotkey2text(e) {
						case conf.Hotkeys.PlayPause:
							err := player.TogglePause()
							if err != nil {
								errl.Println(err)
								dialog.Message("Error changing pause state").Error()
							}
						case conf.Hotkeys.SwitchTab:
							var t int
							for i, tab := range ui.TabChooser.Tabs {
								if tab.Key == ui.TabChooser.Group.Value {
									t = i
									break
								}
							}
							ui.TabChooser.Group.Value = ui.TabChooser.Tabs[(t+1)%len(ui.TabChooser.Tabs)].Key
						case conf.Hotkeys.SwitchTabBack:
							var t int
							for i, tab := range ui.TabChooser.Tabs {
								if tab.Key == ui.TabChooser.Group.Value {
									t = i
									break
								}
							}
							var newt = t - 1
							if newt < 0 {
								newt += len(ui.TabChooser.Tabs)
							}
							ui.TabChooser.Group.Value = ui.TabChooser.Tabs[newt].Key
						case conf.Hotkeys.VolumeUp:
							pl, err := player.Player()
							if err != nil {
								errl.Println(err)
								dialog.Message("Error changing volume").Error()
							}
							vol, err := pl.Volume()
							if err != nil {
								errl.Println(err)
								dialog.Message("Error changing volume").Error()
							}
							vol += 5
							if vol > 100 {
								vol = 100
							}
							err = pl.SetVolume(vol)
							if err != nil {
								errl.Println(err)
								dialog.Message("Error changing volume").Error()
							}
						case conf.Hotkeys.VolumeDown:
							pl, err := player.Player()
							if err != nil {
								errl.Println(err)
								dialog.Message("Error changing volume").Error()
							}
							vol, err := pl.Volume()
							if err != nil {
								errl.Println(err)
								dialog.Message("Error changing volume").Error()
							}
							vol -= 5
							if vol < 0 {
								vol = 0
							}
							err = pl.SetVolume(vol)
							if err != nil {
								errl.Println(err)
								dialog.Message("Error changing volume").Error()
							}
						case conf.Hotkeys.QuitApp:
							trayQuit()
						}
					}
				}

				paint.ColorOp{Color: ui.Theme.Palette.Bg}.Add(&ops)
				paint.PaintOp{}.Add(&ops)

				ui.Layout(gtx)
				e.Frame(gtx.Ops)

			case system.DestroyEvent:
				if conf.CloseAppWhenWindowCloses {
					defer trayQuit()
				}
				return e.Err
			}
		}
	}
}

// Layout show interface
func (ui *UI) Layout(gtx C) D {
	return layout.Flex{
		Axis: layout.Vertical,
	}.Layout(gtx,
		layout.Rigid(func(gtx C) D {
			// no (widget.Enum).Changed() because of hotkeys
			if ui.TabChooser.Group.Value != "_settings" && ui.TabChooser.Group.Value != chosenTab {
				err := audioSetSourceURL(getStationByName(ui.TabChooser.Group.Value).Link)
				if err != nil {
					errl.Println(err)
					dialog.Message("Error getting data about radio").Error()
				}
				StationChanging.SetStr(ui.TabChooser.Group.Value)
				chosenTab = ui.TabChooser.Group.Value
			}
			return ui.TabChooser.Layout(gtx)
		}),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				if ui.TabChooser.Group.Value == "_settings" {
					return ui.SettingsActivity.Layout(gtx, ui.Theme)
				}
				return ui.RadioActivity.Layout(gtx, ui.Theme, ui.TabChooser.Group.Value)
			},
		),
	)
}

// TabChooser is addition to tabs module
type TabChooser struct {
	List  material.ListStyle
	Group *widget.Enum
	Tabs  []tabs.TabStyle
}

// NewTabChooser returns ptr to initialized TabChooser
func NewTabChooser(th *material.Theme) *TabChooser {
	ch := new(TabChooser)
	ch.List = material.List(
		th,
		new(widget.List),
	)

	ch.Group = new(widget.Enum)
	ch.Tabs = make([]tabs.TabStyle, 0)
	return ch
}

// Layout shows TabChooser's interface
func (ch *TabChooser) Layout(gtx C) D {
	return layout.UniformInset(unit.Dp(5)).Layout(
		gtx,
		func(gtx C) D {
			return layout.Center.Layout(gtx,
				func(gtx C) D {
					return ch.List.Layout(
						gtx,
						len(ch.Tabs),
						func(gtx C, index int) D {
							return layout.Flex{
								Axis: layout.Horizontal,
							}.Layout(
								gtx,
								layout.Rigid(ch.Tabs[index].Layout),
								layout.Rigid(layout.Spacer{Width: unit.Dp(2.5)}.Layout),
							)
						},
					)
				},
			)
		},
	)
}

// RadioActivity implements radio player screen
type RadioActivity struct {
	PlayButton   material.ButtonStyle
	VolumeSlider material.SliderStyle
}

// NewRadioActivity returns ptr to initialized RadioActivity
func NewRadioActivity(th *material.Theme) *RadioActivity {
	ra := new(RadioActivity)
	ra.PlayButton = material.Button(
		th,
		new(widget.Clickable),
		"Pause",
	)
	ra.VolumeSlider = material.Slider(
		th,
		&widget.Float{
			Value: 100,
		},
		0,
		100,
	)
	return ra
}

// Layout shows RadioActivity's interface
func (ra *RadioActivity) Layout(gtx C, th *material.Theme, stationName string) D {
	return layout.Flex{
		Axis: layout.Vertical,
	}.Layout(gtx,
		layout.Rigid(
			func(gtx C) D {
				return layout.Center.Layout(gtx, material.H3(th, stationName).Layout)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(10)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				ra.PlayButton.Text = playPauseText()
				if ra.PlayButton.Button.Clicked() {
					err := player.TogglePause()
					if err != nil {
						errl.Println(err)
						dialog.Message("Error changing pause state").Error()
					}
				}
				return layout.UniformInset(unit.Dp(5)).Layout(gtx, ra.PlayButton.Layout)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				if ra.VolumeSlider.Float.Changed() {
					err := audioSetVolume(int(ra.VolumeSlider.Float.Value))
					if err != nil {
						errl.Println(err)
						dialog.Message("Error setting volume").Error()
					}
				} else if VolumeChanging.Changed() {
					ra.VolumeSlider.Float.Value = float32(VolumeChanging.GetInt())
				}
				return layout.UniformInset(unit.Dp(15)).Layout(
					gtx,
					func(gtx C) D {
						return layout.Flex{
							Axis: layout.Horizontal,
						}.Layout(gtx,
							layout.Rigid(
								func(gtx C) D {
									lbl := material.H5(th,
										fmt.Sprintf("Volume: %s%%",
											goyystr.PadRight(
												strconv.Itoa(
													int(
														ra.VolumeSlider.Float.Value,
													),
												),
												3, " ",
											),
										),
									)
									return lbl.Layout(gtx)
								},
							),
							layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
							layout.Rigid(
								func(gtx C) D {
									gtx.Constraints.Min.X = gtx.Constraints.Max.X - 15
									return ra.VolumeSlider.Layout(gtx)
								},
							),
						)
					},
				)
			},
		),
	)
}

// RadioActivity implements settings screen
type SettingsActivity struct {
	List material.ListStyle

	Elements []layout.Widget

	NotHideToTrayBox     material.CheckBoxStyle
	ThemeChooser         *ThemeChooser
	StationsList         *StationsList
	HotkeysList          *HotkeysList
	ResetToDefaultButton material.ButtonStyle

	AppInfoList       material.ListStyle
	RepoLinkClickable *widget.Clickable
}

// NewRadioActivity returns ptr to initialized SettingsActivity
func NewSettingsActivity(th *material.Theme) *SettingsActivity {
	sa := &SettingsActivity{}
	sa.List = material.List(
		th,
		&widget.List{
			List: layout.List{
				Axis: layout.Vertical,
			},
		},
	)

	sa.NotHideToTrayBox = material.CheckBox(
		th,
		&widget.Bool{
			Value: conf.CloseAppWhenWindowCloses,
		},
		"Do not hide to tray when closing window",
	)

	sa.ThemeChooser = NewThemeChooser(th)

	sa.StationsList = NewStationsList(th)

	sa.HotkeysList = NewHotkeysList(th)

	sa.ResetToDefaultButton = material.Button(
		th,
		new(widget.Clickable),
		"Reset to default settings",
	)

	sa.AppInfoList = material.List(
		th,
		&widget.List{
			List: layout.List{
				Axis: layout.Horizontal,
			},
		},
	)
	sa.RepoLinkClickable = new(widget.Clickable)

	sa.Elements = []layout.Widget{
		func(gtx C) D {
			if sa.NotHideToTrayBox.CheckBox.Changed() {
				conf.CloseAppWhenWindowCloses = sa.NotHideToTrayBox.CheckBox.Value
				QuitOnWinClosingChanging.SetBool(sa.NotHideToTrayBox.CheckBox.Value)
				err := confSave()
				if err != nil {
					errl.Println(err)
					dialog.Message("Error saving settings").Error()
				}
			}
			return sa.NotHideToTrayBox.Layout(gtx)
		},
		func(gtx C) D {
			return sa.ThemeChooser.Layout(gtx, th)
		},
		func(gtx C) D {
			return sa.StationsList.Layout(gtx, th)
		},
		func(gtx C) D {
			return sa.HotkeysList.Layout(gtx, th)
		},
		func(gtx C) D {
			if sa.ResetToDefaultButton.Button.Clicked() {
				if yes := dialog.Message("Are you sure about reseting settings?").YesNo(); yes {
					conf = defaultConf
					err := confSave()
					if err != nil {
						errl.Println(err)
						dialog.Message("Error saving settings").Error()
					} else {
						*sa = *NewSettingsActivity(th)
					}
				}

			}
			return layout.Flex{Axis: layout.Vertical}.Layout(gtx,
				layout.Rigid(layout.Spacer{Height: unit.Dp(10)}.Layout),
				layout.Rigid(sa.ResetToDefaultButton.Layout),
			)
		},
		func(gtx C) D {
			divider := material.H4(th, " | ").Layout

			var elements []layout.Widget = []layout.Widget{
				material.H4(th, "Lofio").Layout,
				divider,
				material.H4(th, appVersion).Layout,
				divider,
				func(gtx C) D {
					if sa.RepoLinkClickable.Clicked() {
						open.Start("https://codeberg.org/hrueschwein/lofio")
					}
					lbl := material.H4(th, "Repository")
					lbl.Color = th.Palette.ContrastBg
					return sa.RepoLinkClickable.Layout(gtx, lbl.Layout)
				},
			}
			return layout.Flex{
				Axis: layout.Vertical,
			}.Layout(gtx,
				layout.Rigid(component.Divider(th).Layout),
				layout.Rigid(
					func(gtx C) D {
						return layout.Center.Layout(gtx, func(gtx C) D {
							return sa.AppInfoList.Layout(
								gtx,
								len(elements),
								func(gtx C, index int) D {
									return elements[index](gtx)
								},
							)
						})
					},
				),
			)
		},
	}

	return sa
}

// Layout shows SettingsActivity's interface
func (sa *SettingsActivity) Layout(gtx C, th *material.Theme) D {
	return layout.UniformInset(unit.Dp(15)).Layout(gtx,
		func(gtx C) D {
			return sa.List.Layout(
				gtx,
				len(sa.Elements),
				func(gtx C, index int) D {
					return layout.Flex{
						Axis: layout.Vertical,
					}.Layout(
						gtx,
						layout.Rigid(sa.Elements[index]),
						layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
					)
				},
			)
		},
	)
}

// StationList implements editor of stations
type StationsList struct {
	List             material.ListStyle
	StationsInputs   []*StationInput
	AddStationButton material.ButtonStyle
	SaveButton       material.ButtonStyle
}

// NewStationsList returns ptr to initialized StationsList
func NewStationsList(th *material.Theme) *StationsList {
	sl := new(StationsList)
	sl.List = material.List(
		th,
		&widget.List{
			List: layout.List{
				Axis: layout.Vertical,
			},
		},
	)
	sl.StationsInputs = make([]*StationInput, 0, len(conf.Stations))
	for _, st := range conf.Stations {
		sl.StationsInputs = append(sl.StationsInputs, NewStationInput(st, th))
	}
	sl.AddStationButton = material.Button(
		th,
		new(widget.Clickable),
		"Add station",
	)
	sl.SaveButton = material.Button(
		th,
		new(widget.Clickable),
		"Save stations",
	)
	return sl
}

// Layout shows StationsList's interface
func (sl *StationsList) Layout(gtx C, th *material.Theme) D {
	if RemoveStationsChanging.Changed() {
		var idx int = RemoveStationsChanging.GetInt();
		sl.StationsInputs = remove(idx, sl.StationsInputs)
	}
	return layout.Flex{
		Axis: layout.Vertical,
	}.Layout(gtx,
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				return layout.Center.Layout(gtx, material.H3(th, "Stations").Layout)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				return sl.List.Layout(gtx, len(sl.StationsInputs),
					func(gtx C, index int) D {
						return layout.Flex{
							Axis: layout.Vertical,
						}.Layout(gtx,
							layout.Rigid(
								func(gtx C) D {
									return sl.StationsInputs[index].Layout(gtx, th, index)
								},
							),
							layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
						)
					},
				)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				if sl.AddStationButton.Button.Clicked() {
					sl.StationsInputs = append(sl.StationsInputs, NewStationInput(Station{}, th))
				}
				return layout.Center.Layout(gtx, sl.AddStationButton.Layout)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				if sl.SaveButton.Button.Clicked() {
					conf.Stations = make([]Station, len(sl.StationsInputs))
					for ind, inp := range sl.StationsInputs {
						conf.Stations[ind].Name = inp.NameInput.Editor.Text()
						conf.Stations[ind].Link = inp.LinkInput.Editor.Text()
					}
					err := confSave()
					if err != nil {
						errl.Println(err)
						dialog.Message("Error saving settings").Error()
					}
					dialog.Message("Re-open window to see changes").Info()
				}
				return layout.Center.Layout(gtx, sl.SaveButton.Layout)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
	)
}

// StationInputs implements one station in StationsList
type StationInput struct {
	NameInput, LinkInput material.EditorStyle
	DeleteButton         material.ButtonStyle
}

// NewStationInputs returns ptr to initialized StationInputs
func NewStationInput(st Station, th *material.Theme) *StationInput {
	si := new(StationInput)
	si.NameInput = material.Editor(
		th,
		&widget.Editor{
			SingleLine: true,
			InputHint:  key.HintText,
		},
		"Station name...",
	)
	si.NameInput.Editor.Insert(st.Name)
	si.LinkInput = material.Editor(
		th,
		&widget.Editor{
			SingleLine: true,
			InputHint:  key.HintURL,
		},
		"Station link...",
	)
	si.LinkInput.Editor.Insert(st.Link)

	si.DeleteButton = material.Button(
		th,
		new(widget.Clickable),
		"Delete",
	)
	si.DeleteButton.Color      = color.NRGBA{0xff, 0xff, 0xff, 0xff}
	si.DeleteButton.Background = color.NRGBA{0xff, 0x00, 0x00, 0xff}

	return si
}

// Layout shows StationInputs's interface
func (si *StationInput) Layout(gtx C, th *material.Theme, ind int) D {
	return layout.Flex{
		Axis: layout.Vertical,
	}.Layout(gtx,
		layout.Rigid(
			func(gtx C) D {
				return layout.Flex{
					Axis: layout.Horizontal,
				}.Layout(gtx,
					layout.Rigid(material.H6(th, "Name:  ").Layout),
					layout.Rigid(func(gtx C) D {
						return widget.Border{
							Color:        th.Palette.Fg,
							CornerRadius: unit.Dp(5),
							Width:        unit.Dp(0.5),
						}.Layout(gtx,
							func(gtx C) D {
								gtx.Constraints.Min.X = gtx.Constraints.Max.X
								return layout.UniformInset(unit.Dp(5)).Layout(gtx, si.NameInput.Layout)
							},
						)
					}),
				)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				return layout.Flex{
					Axis: layout.Horizontal,
				}.Layout(gtx,
					layout.Rigid(material.H6(th, "Link:  ").Layout),
					layout.Rigid(func(gtx C) D {
						return widget.Border{
							Color:        th.Palette.Fg,
							CornerRadius: unit.Dp(5),
							Width:        unit.Dp(0.5),
						}.Layout(gtx,
							func(gtx C) D {
								gtx.Constraints.Min.X = gtx.Constraints.Max.X
								return layout.UniformInset(unit.Dp(5)).Layout(gtx, si.LinkInput.Layout)
							},
						)
					}),
				)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				if si.DeleteButton.Button.Clicked() {
					if yes := dialog.Message(
						"Are you sure you want remove station " + si.NameInput.Editor.Text() + "?",
					).YesNo(); yes {
						RemoveStationsChanging.SetInt(ind)
					}
				}
				return layout.UniformInset(unit.Dp(15)).Layout(gtx, si.DeleteButton.Layout)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
	)
}

type HotkeysList struct {
	List          material.ListStyle
	HotkeysInputs []*HotkeyInput
	SaveButton    material.ButtonStyle
}

func NewHotkeysList(th *material.Theme) *HotkeysList {
	var hl = HotkeysList{}
	hl.List = material.List(
		th,
		&widget.List{
			List: layout.List{
				Axis: layout.Vertical,
			},
		},
	)
	hl.HotkeysInputs = []*HotkeyInput{
		NewHotkeyInput(
			"play_pause",
			conf.Hotkeys.PlayPause,
			"Play/Pause",
			th,
		),
		NewHotkeyInput(
			"switch_tab",
			conf.Hotkeys.SwitchTab,
			"Switch tab",
			th,
		),
		NewHotkeyInput(
			"switch_tab_back",
			conf.Hotkeys.SwitchTabBack,
			"Switch tab back",
			th,
		),
		NewHotkeyInput(
			"volume_up",
			conf.Hotkeys.VolumeUp,
			"Volume up",
			th,
		),
		NewHotkeyInput(
			"volume_down",
			conf.Hotkeys.VolumeUp,
			"Volume down",
			th,
		),
		NewHotkeyInput(
			"quit_app",
			conf.Hotkeys.QuitApp,
			"Quit app",
			th,
		),
	}

	hl.SaveButton = material.Button(
		th,
		new(widget.Clickable),
		"Save hotkeys",
	)
	return &hl
}

func (hl *HotkeysList) Layout(gtx C, th *material.Theme) D {
	return layout.Flex{
		Axis: layout.Vertical,
	}.Layout(
		gtx,
		layout.Rigid(layout.Spacer{Height: unit.Dp(15)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				return layout.Center.Layout(gtx, material.H3(th, "Hotkeys").Layout)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				return hl.List.Layout(gtx, len(hl.HotkeysInputs),
					func(gtx C, index int) D {
						return layout.Flex{
							Axis: layout.Vertical,
						}.Layout(gtx,
							layout.Rigid(
								func(gtx C) D {
									return hl.HotkeysInputs[index].Layout(gtx, th)
								},
							),
							layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
						)
					},
				)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(
			func(gtx C) D {
				if hl.SaveButton.Button.Clicked() {
					conf.Hotkeys = HotkeysConfig{
						PlayPause:     hl.HotkeysInputs[0].Value,
						SwitchTab:     hl.HotkeysInputs[1].Value,
						SwitchTabBack: hl.HotkeysInputs[2].Value,
						VolumeUp:      hl.HotkeysInputs[3].Value,
						VolumeDown:    hl.HotkeysInputs[4].Value,
						QuitApp:       hl.HotkeysInputs[5].Value,
					}
					err := confSave()
					if err != nil {
						dialog.Message("Error saving config").Error()
						errl.Println(err)
					}
					debl.Println(conf.Hotkeys)
				}
				return layout.UniformInset(unit.Dp(25)).Layout(gtx, hl.SaveButton.Layout)
			},
		),
		layout.Rigid(layout.Spacer{Height: unit.Dp(15)}.Layout),
	)
}

type HotkeyInput struct {
	Key, Value, Text string
	KeyInputButton   material.ButtonStyle
}

func NewHotkeyInput(key_, value, text string, th *material.Theme) *HotkeyInput {
	hi := HotkeyInput{
		Key:   key_,
		Value: value,
		Text:  text,
	}

	hi.KeyInputButton = material.Button(
		th,
		new(widget.Clickable),
		value,
	)
	return &hi
}

func (hi *HotkeyInput) Layout(gtx C, th *material.Theme) D {
	const allKeys = key.Set("(Ctrl)-(Shift)-(Alt)-(⌘)-" +
		"[A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,;,',.,/,\\,"+
		"1,2,3,4,5,6,7,8,9,0,=,!,@,#,$,%,^,&,*,(,),_,+,{,},:,\",<,>,?,←,→,↓,↑," +
		"Space,Tab,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12,Back,⎋,⇱,⇲,⌦,⌫,⇞,⇟]",
	)
	// No square braces, minus and comma because of key set syntax.


	return layout.Flex{
		Axis:    layout.Horizontal,
		Spacing: layout.SpaceBetween,
	}.Layout(
		gtx,
		layout.Rigid(material.H6(th, hi.Text+": ").Layout),
		layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
		layout.Rigid(func(gtx C) D {
			if hi.KeyInputButton.Button.Clicked() {
				win := app.NewWindow(
					app.Title("Input hotkey.."),
					app.Size(unit.Dp(480), unit.Dp(480)),
					app.MinSize(unit.Dp(480), unit.Dp(480)),
					app.MaxSize(unit.Dp(480), unit.Dp(480)),
				)
				{
					var hops op.Ops

				WIN_FOR:
					for {
						select {
						case e := <-win.Events():
							switch e := e.(type) {
							case system.FrameEvent:
								hgtx := layout.NewContext(&hops, e)

								key.InputOp{
									Tag:  win,
									Keys: allKeys,
								}.Add(hgtx.Ops)

								for _, e := range hgtx.Events(win) {
									switch e := e.(type) {
									case key.Event:

										if e.State != key.Release {
											continue
										}

										if e.Modifiers.String() != "" && e.Name != key.NameEscape {
											hi.Value = hotkey2text(e)
											hi.KeyInputButton.Text = readableKeyNames(hi.Value)
										}
										win.Perform(system.ActionClose)
									}
								}

								paint.ColorOp{Color: th.Palette.Bg}.Add(&hops)
								paint.PaintOp{}.Add(&hops)

								layout.UniformInset(unit.Dp(15)).Layout(hgtx, material.H3(th, "Input hotkey... \n(Press Esc to cancel)").Layout)

								e.Frame(hgtx.Ops)

							case system.DestroyEvent:
								break WIN_FOR
							}
						}
					}
				}
			}

			return layout.UniformInset(unit.Dp(5)).Layout(
				gtx,
				func(gtx C) D {
					gtx.Constraints.Max.X = 150
					gtx.Constraints.Min.X = gtx.Constraints.Max.X
					return hi.KeyInputButton.Layout(gtx)
				},
			)
		}),
	)
}

type ThemeChooser struct {
	Group *widget.Enum
	
	RadioButtons []material.RadioButtonStyle
}

func NewThemeChooser(th *material.Theme) *ThemeChooser {
	var tc ThemeChooser
	
	tc.Group = new(widget.Enum)
	
	tc.RadioButtons = []material.RadioButtonStyle{
		material.RadioButton(
			th,
			tc.Group,
			"auto",
			"Auto-detect theme",
		),
		material.RadioButton(
			th,
			tc.Group,
			"light",
			"Light theme",
		),
		material.RadioButton(
			th,
			tc.Group,
			"dark",
			"Dark theme",
		),
	}

	switch conf.Theme	{
	case 0:
		tc.Group.Value = "auto"
	case 1:
		tc.Group.Value = "light"
	case 2:
		tc.Group.Value = "dark"
	}

	return &tc
}

func (tc *ThemeChooser) Layout(gtx C, th *material.Theme) D {
	return layout.Flex{
		Axis: layout.Vertical,
	}.Layout(gtx,
		layout.Rigid(material.H6(th, "Theme: ").Layout),
		layout.Rigid(
			func(gtx C) D {
				if tc.Group.Changed() {
					switch tc.Group.Value {
					case "auto":
						conf.Theme = 0
					case "light":
						conf.Theme = 1
					case "dark":
						conf.Theme = 2
					}
					err := confSave()
					if err != nil {
						errl.Println(err)
						dialog.Message("Error while saving in config").Error()
					}		
					dialog.Message("Re-open window to see changes").Info()
				}
				var options = make([]layout.FlexChild, len(tc.RadioButtons))
				for i, o := range tc.RadioButtons {
					options[i] = layout.Rigid(o.Layout)
				}
				return layout.Flex{
					Axis: layout.Horizontal,
				}.Layout(gtx,
					options...,
				)
			},
		),
	)
}
