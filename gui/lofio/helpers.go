package main

import (
	"gioui.org/io/key"
	"strings"
)

func hotkey2text(e key.Event) string {
	var name string = e.Name

	if e.Modifiers.String() != ""  {
		return e.Modifiers.String() + "-" + name
	}
	return name
}

func multiReplace(orig string, templ map[string]string) string {
	var res string = orig
	for t, r := range templ {
		res = strings.Replace(res, t, r, -1)
	}
	return res
}

func readableKeyNames(orig string) string {
	return multiReplace(orig,
		map[string]string{
			key.NameReturn:          "Return",
			key.NameEnter:           "Enter",
			key.NameEscape:          "Escape",
			key.NameHome:            "Home",
			key.NameEnd:             "End",
			key.NameDeleteBackward:  "Backspace",
			key.NameDeleteForward:   "Delete",
			key.NamePageUp:          "PgUp",
			key.NamePageDown:        "PgDown",
			key.NameCommand:         "Cmd",
		},
	)
}

func playPauseText() string {
	if player.IsPlaying() {
		return "Pause"
	}
	return "Play"
}

func getStationByName(name string) Station {
	for _, st := range conf.Stations {
		if st.Name == name {
			return st
		}
	}
	return Station{}
}

// yes, stupid and strange, but it works better
// sorry for generics
func remove[T any](ind int, arr []T) []T {
	if ind >= len(arr) || ind < 0 {
		return arr
	}
	var res = make([]T, len(arr)-1)
	var j int
	for i, _ := range arr {
		if i == ind {
			continue
		}
		res[j] = arr[i]
		j++
	}
	return res
}

// Changing helps to track was value changed or not
type Changing struct {
	strVal    string
	intVal    int
	boolVal   bool
	changed   bool
	dub       *Changing
	changedCh chan struct{}
}

// NewChanging returns pointer to Changing with standart values
func NewChanging() *Changing {
	ch := new(Changing)
	ch.changedCh = make(chan struct{})
	return ch
}

func NewDubChanging(dubFrom *Changing) *Changing {
	ch := NewChanging()
	if dubFrom != nil {
		dubFrom.DublicateTo(ch)
	}
	return ch
}

func (ch *Changing) DublicateTo(chptr *Changing) {
	ch.dub = chptr
}

// SetStr sets string value
func (ch *Changing) SetStr(s string) {
	ch.strVal = s
	ch.changed = true
	if len(ch.changedCh) == 0 {
		go func() {
			ch.changedCh <- struct{}{}
			ch.changed = false
		}()
	}
	if ch.dub != nil {
		ch.dub.SetStr(s)
	}
}

// SetInt sets int value
func (ch *Changing) SetInt(i int) {
	ch.intVal = i
	ch.changed = true
	if len(ch.changedCh) == 0 {
		go func() {
			ch.changedCh <- struct{}{}
			ch.changed = false
		}()
	}
	if ch.dub != nil {
		ch.dub.SetInt(i)
	}
}

// SetBool sets bool value
func (ch *Changing) SetBool(b bool) {
	ch.boolVal = b
	ch.changed = true
	if len(ch.changedCh) == 0 {
		go func() {
			ch.changedCh <- struct{}{}
			ch.changed = false
		}()
	}
	if ch.dub != nil {
		ch.dub.SetBool(b)
	}
}

// GetStr returns string value
func (ch *Changing) GetStr() string {
	return ch.strVal
}

// GetInt returns int value
func (ch *Changing) GetInt() int {
	return ch.intVal
}

// GetBool returns bool value
func (ch *Changing) GetBool() bool {
	return ch.boolVal
}

// Changed returns was value changed
// since last check or not
func (ch *Changing) Changed() bool {
	defer func() {
		ch.changed = false
		if len(ch.changedCh) != 0 {
			<-ch.changedCh
		}
	}()
	return ch.changed
}

func (ch *Changing) ChangedCh() <-chan struct{} {
	return ch.changedCh
}

// Some changings
var (
	VolumeChanging           = NewChanging()
	StationChanging          = NewChanging()
	QuitOnWinClosingChanging = NewChanging()
	PausableChanging         = NewChanging()
	RemoveStationsChanging   = NewChanging()

	DubStationChanging = NewDubChanging(StationChanging)
	DubVolumeChanging  = NewDubChanging(VolumeChanging)
)
