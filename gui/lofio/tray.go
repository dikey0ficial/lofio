package main

import (
	"codeberg.org/hrueschwein/lofio/gui/lofio/icon"
	"fyne.io/systray"
)

func trayRun() {
	systray.Run(trayOnReady, trayOnExit)
}

func trayOnReady() {
	systray.SetIcon(icon.Icon)
	systray.SetTitle("Lofio")
	systray.SetTooltip("Lofio")
	stationL := systray.AddMenuItem("Now playing: -", "Now playing station")
	stationL.Disable()
	systray.AddSeparator()
	togglePauseB := systray.AddMenuItem("Play/Pause", "Play/Pause")
	openB := systray.AddMenuItem("Open", "Open Lofio app")
	if conf.CloseAppWhenWindowCloses {
		openB.Disable()
	}
	systray.AddSeparator()
	quitB := systray.AddMenuItem("Quit", "Stop Lofio end exit")
	go func() {
		for {
			select {
			case <-quitB.ClickedCh:
				debl.Println("quiting...")
				systray.Quit()
				break
			case <-openB.ClickedCh:
				if !IsWindowOpenned {
					debl.Println("openning")
					go newWindow()
				}
			case <-togglePauseB.ClickedCh:
				err := player.TogglePause()
				if err != nil {
					errl.Println(err)
				}
			case <-StationChanging.ChangedCh():
				stationL.SetTitle("Now playing: " + StationChanging.GetStr())
			case <-QuitOnWinClosingChanging.ChangedCh():
				if QuitOnWinClosingChanging.GetBool() {
					openB.Disable()
				} else {
					openB.Enable()
				}
			}
		}
	}()
}

func trayQuit() {
	systray.Quit()
}

func trayOnExit() {
	// there will be something
}
