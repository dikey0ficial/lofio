package main

import (
	"gioui.org/app"
	"gioui.org/unit"
)

var (
	IsWindowOpenned bool
)

func newWindow() error {
	options := []app.Option{
		app.Title("Lofio"),
		app.Size(unit.Dp(480), unit.Dp(640)),
		app.MinSize(unit.Dp(480), unit.Dp(480)),
	}
	win := app.NewWindow(options...)

	ui := NewUI()
	IsWindowOpenned = true
	err := ui.Run(win)

	if err != nil {
		return err
	}
	IsWindowOpenned = false
	return nil
}
