package main

import (
	vlc "github.com/adrg/libvlc-go/v3"
)

var (
	player    *vlc.ListPlayer
	mediaList *vlc.MediaList
)

func audioInit() error {
	tags := []string{"--no-video", "--quiet"}
	if conf.debug {
		tags[1] = ""
	}
	err := vlc.Init(tags...)
	if err != nil {
		return err
	}
	err = vlc.SetAppName("Lofio", "Lofio LibVLC/")
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}
	player, err = vlc.NewListPlayer()
	if err != nil {
		return err
	}
	mediaList, err = vlc.NewMediaList()
	if err != nil {
		return err
	}
	err = player.SetMediaList(mediaList)
	if err != nil {
		return err
	}
	if len(conf.Stations) != 0 {
		err = audioSetSourceURL(conf.Stations[0].Link)
		if err != nil {
			return err
		}
		StationChanging.SetStr(conf.Stations[0].Name)

		err = player.Play()
		if err != nil {
			return err
		}

	}
	{

		pl, err := player.Player()
		if err != nil {
			return err
		}

		evMgr, err := pl.EventManager()
		if err != nil {
			return err
		}
		_, err = evMgr.Attach(vlc.MediaPlayerAudioVolume, MediaPlayerAudioVolumeHandler, nil)
		if err != nil {
			return err
		}
		_, err = evMgr.Attach(vlc.MediaPlayerPaused, MediaPlayerPauseToogledHandler, nil)
		if err != nil {
			return err
		}
		_, err = evMgr.Attach(vlc.MediaPlayerPlaying, MediaPlayerPauseToogledHandler, nil)
		if err != nil {
			return err
		}
	}

	go func() {
		<-stopCh
		if player != nil {
			player.Stop()
			player.Release()
		}
		if mediaList != nil {
			mediaList.Release()
		}
	}()
	return nil
}

// ==================== Audio helpers ====================
func audioClearList() error {
	c, err := mediaList.Count()
	if err != nil {
		return err
	}
	for ; c > 0; c-- {
		err = mediaList.RemoveMediaAtIndex(uint(c - 1))
		if err != nil {
			return err
		}
	}
	return nil
}

func audioSetSourceURL(url string) error {
	if err := audioClearList(); err != nil {
		return err
	}
	err := player.Stop()
	if err != nil {
		return err
	}
	err = mediaList.AddMediaFromURL(url)
	if err != nil {
		return err
	}
	return player.Play()
}

func audioSetVolume(newVol int) error {
	pl, err := player.Player()
	if err != nil {
		return err
	}
	return pl.SetVolume(newVol)
}

// =======================================================

// ==================== Event handlers ====================

func MediaPlayerAudioVolumeHandler(vlc.Event, interface{}) {
	pl, err := player.Player()
	if err != nil {
		return
	}
	vol, err := pl.Volume()
	if err != nil {
		return
	}
	VolumeChanging.SetInt(vol)
}

func MediaPlayerPauseToogledHandler(vlc.Event, interface{}) {
	PausableChanging.SetInt(-1)
}

// ===================================================
