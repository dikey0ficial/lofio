//go:build !nompris

package main

import (
	_ "embed"
	"time"

	dbus "github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/introspect"
)

var (
	dbusConn    *dbus.Conn
	mprisPlayer *MprisPlayer
	busName     = "org.mpris.MediaPlayer2.lofio"
	mdata       Metadata

	//go:embed ifaces.xml
	mediaPlayerInterfaces string
)

func init() {
	if inits != nil {
		inits["mpris"] = mprisInit
	}
	if mains != nil {
		mains["mpris"] = mprisMain
	}
}

func mprisInit() error {
	{
		conn, err := dbus.ConnectSessionBus()
		if err != nil {
			return err
		} else if conn == nil {
			return ErrNilDBusConn
		}
		dbusConn = conn
	}

	_, err := dbusConn.RequestName(busName, dbus.NameFlagDoNotQueue)
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}

	err = dbusConn.Export(
		mprisPlayer,
		dbus.ObjectPath(

			"/org/mpris/MediaPlayer2",
		),
		"org.mpris.MediaPlayer2",
	)
	if err != nil {
		return err
	}
	err = dbusConn.Export(
		mprisPlayer,
		dbus.ObjectPath(
			"/org/mpris/MediaPlayer2",
		),
		"org.mpris.MediaPlayer2.Player",
	)
	if err != nil {
		return err
	}
	err = dbusConn.Export(
		mprisPlayer,
		dbus.ObjectPath(

			"/org/mpris/MediaPlayer2",
		),
		"org.freedesktop.DBus.Peer",
	)
	if err != nil {
		return err
	}
	err = dbusConn.Export(
		mprisPlayer,
		dbus.ObjectPath(
			"/org/mpris/MediaPlayer2",
		),
		"org.freedesktop.DBus.Properties",
	)
	if err != nil {
		return err
	}
	err = dbusConn.Export(
		introspect.Introspectable(mediaPlayerInterfaces),
		dbus.ObjectPath(
			"/org/mpris/MediaPlayer2",
		),
		"org.freedesktop.DBus.Introspectable",
	)
	if err != nil {
		return err
	}

	mprisPlayer = NewMprisPlayer()

	go func() {
		<-stopCh
		if dbusConn != nil {
			dbusConn.ReleaseName(busName)
			dbusConn.Close()
		}
	}()
	return nil
}

type Metadata struct {
	TrackID string
	Title   string        // music title
	Artist  string        // music artist
	Length  time.Duration // length of music, when representing for dbus itll be in microseconds
	Genres  []string
	BPM     int
	ArtURL  string
}

func (m *Metadata) toMap() map[string]interface{} {
	var mprisMap = make(map[string]interface{})
	if m.TrackID != "" {
		mprisMap["mpris:trackid"] = dbus.ObjectPath(m.TrackID)
	}
	if m.Title != "" {
		mprisMap["xesam:title"] = m.Title
	}
	if m.Artist != "" {
		mprisMap["xesam:artist"] = []string{m.Artist}
	}
	if m.Length != 0 {
		mprisMap["mpris:length"] = m.Length.Microseconds()
	}
	if len(m.Genres) != 0 {
		mprisMap["xesam:genre"] = m.Genres
	}
	if m.BPM != 0 {
		mprisMap["xesam:audioBPM"] = m.BPM
	}
	if m.ArtURL != "" {
		mprisMap["mpris:artUrl"] = m.ArtURL
	}
	return mprisMap
}

type MprisPlayer struct {
}

func NewMprisPlayer() *MprisPlayer {
	mp := MprisPlayer{}
	return &mp
}

func (p *MprisPlayer) PlaybackStatus() string {
	if player.IsPlaying() {
		return "Playing"
	}
	return "Paused"

}

// ==================== Peer ====================

func (p *MprisPlayer) Ping() *dbus.Error {
	return nil
}

// ==============================================

// ==================== MediaPlayer2 ====================

func (p *MprisPlayer) Raise() *dbus.Error {
	if !IsWindowOpenned {
		go newWindow()
	}
	return nil
}

func (p *MprisPlayer) Quit() *dbus.Error {
	trayQuit()
	return nil
}

// ======================================================

// ==================== Player ====================

func (p *MprisPlayer) Next() *dbus.Error {
	return nil
}

func (p *MprisPlayer) Previous() *dbus.Error {
	return nil
}

func (p *MprisPlayer) Pause() *dbus.Error {
	pl, err := player.Player()
	if err != nil {
		return dbus.NewError(
			err.Error(),
			[]interface{}{},
		)
	}
	err = pl.SetPause(true)
	if err != nil {
		return dbus.NewError(
			err.Error(),
			[]interface{}{},
		)
	}
	return nil
}

func (p *MprisPlayer) Play() *dbus.Error {
	pl, err := player.Player()
	if err != nil {
		return dbus.NewError(
			err.Error(),
			[]interface{}{},
		)
	}
	err = pl.SetPause(false)
	if err != nil {
		return dbus.NewError(
			err.Error(),
			[]interface{}{},
		)
	}
	return nil
}

func (p *MprisPlayer) PlayPause() *dbus.Error {
	pl, err := player.Player()
	if err != nil {
		return dbus.NewError(
			err.Error(),
			[]interface{}{},
		)
	}
	err = pl.TogglePause()
	if err != nil {
		return dbus.NewError(
			err.Error(),
			[]interface{}{},
		)
	}
	return nil
}

func (p *MprisPlayer) Stop() *dbus.Error {
	err := player.Stop()
	if err != nil {
		return dbus.NewError(
			err.Error(),
			[]interface{}{},
		)
	}
	return nil
}

func (p *MprisPlayer) OpenMediaPicker() *dbus.Error {
	return nil
}

func (p *MprisPlayer) OpenMediaManager() *dbus.Error {
	return nil
}

func (p *MprisPlayer) Seek(x int) *dbus.Error {
	return nil
}

func (p *MprisPlayer) SetPosition(o dbus.ObjectPath, x int) *dbus.Error {
	return nil
}

func (p *MprisPlayer) OpenUri(s string) *dbus.Error {
	return nil
}

// ================================================

//  ==================== Properties ====================

func (p *MprisPlayer) Get(iface, prop string) (dbus.Variant, *dbus.Error) {
	switch prop {
	case "Metadata":
		return dbus.MakeVariant(mdata.toMap()), nil
	case "Identity":
		return dbus.MakeVariant("Lofio"), nil
	case "Position":
		return dbus.MakeVariant(int64(0)), nil
	case "PlaybackStatus":
		return dbus.MakeVariant(p.PlaybackStatus()), nil
	case "Rate":
		return dbus.MakeVariant(float64(1)), nil
	case "MaximumRate":
		return dbus.MakeVariant(float64(1)), nil
	case "MinimumRate":
		return dbus.MakeVariant(float64(1)), nil
	case "CanGoNext":
		return dbus.MakeVariant(false), nil
	case "CanGoPrevious":
		return dbus.MakeVariant(false), nil
	case "CanSeek":
		return dbus.MakeVariant(false), nil
	case "CanControl":
		return dbus.MakeVariant(true), nil
	case "CanPlay":
		return dbus.MakeVariant(true), nil
	case "CanPause":
		return dbus.MakeVariant(true), nil
	case "CanControlVolume":
		return dbus.MakeVariant(true), nil
	case "Volume":
		return dbus.MakeVariant(float64(DubVolumeChanging.GetInt()) / 100), nil
	case "HasMediaPicker":
		return dbus.MakeVariant(false), nil
	case "PlayerType":
		return dbus.MakeVariant("music player"), nil

	case "CanQuit":
		return dbus.MakeVariant(true), nil
	case "CanRaise":
		return dbus.MakeVariant(true), nil
	case "HasTrackList":
		return dbus.MakeVariant(false), nil
	case "SupportedUriSchemes":
		return dbus.MakeVariant([]string{}), nil
	case "SupportedMimeTypes":
		return dbus.MakeVariant([]string{}), nil

	default:
		return dbus.MakeVariant(""), nil
	}
}

func (p *MprisPlayer) Set(iface, prop string, value dbus.Variant) *dbus.Error {
	switch prop {
	case "Rate":
		return nil
	case "Volume":
		pl, err := player.Player()
		if err != nil {
			return dbus.NewError(
				err.Error(),
				[]interface{}{},
			)
		}
		val, ok := value.Value().(float64)
		if !ok {
			return dbus.NewError(
				"wrong type value",
				[]interface{}{},
			)
		}
		err = pl.SetVolume(int(val * 100))
		if err != nil {
			return dbus.NewError(
				err.Error(),
				[]interface{}{},
			)
		}
		return nil
	default:
		return nil
	}
}

func (p *MprisPlayer) GetAll(iface string) (map[string]dbus.Variant, *dbus.Error) {
	return map[string]dbus.Variant{
		"Metadata":            dbus.MakeVariant(mdata.toMap()),
		"Identity":            dbus.MakeVariant("Lofio"),
		"Position":            dbus.MakeVariant(int64(0)),
		"PlaybackStatus":      dbus.MakeVariant(p.PlaybackStatus()),
		"Rate":                dbus.MakeVariant(float64(1)),
		"MaximumRate":         dbus.MakeVariant(float64(1)),
		"MinimumRate":         dbus.MakeVariant(float64(1)),
		"CanGoNext":           dbus.MakeVariant(false),
		"CanGoPrevious":       dbus.MakeVariant(false),
		"CanSeek":             dbus.MakeVariant(false),
		"CanControl":          dbus.MakeVariant(true),
		"CanPlay":             dbus.MakeVariant(true),
		"CanPause":            dbus.MakeVariant(true),
		"CanControlVolume":    dbus.MakeVariant(true),
		"HasMediaPicker":      dbus.MakeVariant(false),
		"PlayerType":          dbus.MakeVariant("music player"),
		"Volume":              dbus.MakeVariant(float64(DubVolumeChanging.GetInt()) / 100),
		"CanQuit":             dbus.MakeVariant(true),
		"CanRaise":            dbus.MakeVariant(true),
		"HasTrackList":        dbus.MakeVariant(false),
		"SupportedUriSchemes": dbus.MakeVariant([]string{}),
		"SupportedMimeTypes":  dbus.MakeVariant([]string{}),
	}, nil
}

// ====================================================

func mprisMain() error {
	mdata.Title = DubStationChanging.GetStr()
	mdata.Artist = "Lofio"
	mdata.TrackID = "/org/mpris/MediaPlayer2/lofio"
	for {
		select {
		case <-DubStationChanging.ChangedCh():
			mdata.Title = DubStationChanging.GetStr()
		case <-PausableChanging.ChangedCh():
			func() {}()
		}
		err := dbusEmit()
		if err != nil {
			return err
		}
	}
}

func dbusEmit() error {
	return dbusConn.Emit(
		dbus.ObjectPath("/org/mpris/MediaPlayer2"),
		"org.freedesktop.DBus.Properties.PropertiesChanged",
		"org.mpris.MediaPlayer2.Player",
		map[string]dbus.Variant{
			"Metadata":       dbus.MakeVariant(mdata.toMap()),
			"PlaybackStatus": dbus.MakeVariant(mprisPlayer.PlaybackStatus()),
		},
		[]string{},
	)
}
