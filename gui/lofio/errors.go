package main

// Error is custom error type
type Error string

// Error implements error interface
func (e Error) Error() string { return string(e) }

// errors
const (
	ErrNilDBusConn = Error("got nil *dbus.Conn")
)
